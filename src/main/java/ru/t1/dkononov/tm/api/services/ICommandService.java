package ru.t1.dkononov.tm.api.services;

import ru.t1.dkononov.tm.api.repository.ICommandRepository;
import ru.t1.dkononov.tm.command.AbstractCommand;
import ru.t1.dkononov.tm.model.Command;

import java.util.Collection;

public interface ICommandService extends ICommandRepository {

}
